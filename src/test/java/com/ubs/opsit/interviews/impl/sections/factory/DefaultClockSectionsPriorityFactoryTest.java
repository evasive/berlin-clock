package com.ubs.opsit.interviews.impl.sections.factory;

import com.ubs.opsit.interviews.impl.entities.BerlinClock;
import com.ubs.opsit.interviews.impl.entities.DefaultBerlinClock;
import com.ubs.opsit.interviews.impl.sections.feautures.SectionColors;
import com.ubs.opsit.interviews.impl.sections.feautures.SectionTypes;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author evasive
 */
public class DefaultClockSectionsPriorityFactoryTest {

    private BerlinClock clock = new DefaultBerlinClock() {
        @Override
        public SectionColors[] getMainHours() {
            return new SectionColors[]{SectionColors.YELLOW};
        }

        @Override
        public SectionColors[] getNestedHours() {
            return new SectionColors[]{SectionColors.WHITE};
        }

        @Override
        public SectionColors[] getMainMinutes() {
            return new SectionColors[]{SectionColors.YELLOW, SectionColors.RED};
        }

        @Override
        public SectionColors[] getNestedMinutes() {
            return new SectionColors[]{SectionColors.YELLOW, SectionColors.WHITE};
        }
    };

    private DefaultClockSectionsPriorityFactory sectionsFactory = new DefaultClockSectionsPriorityFactory(clock);

    @Test
    public void shouldCheckCorrectlySelectionForMinutes() {
        ClockSections sections = sectionsFactory.getClockSections(SectionTypes.MINUTES);
        Assert.assertArrayEquals(new SectionColors[]{SectionColors.YELLOW, SectionColors.RED}, sections.getMainSection());
        Assert.assertArrayEquals(new SectionColors[]{SectionColors.YELLOW, SectionColors.WHITE}, sections.getNestedSection());
    }

    @Test
    public void shouldCheckCorrectlySelectionForHours() {
        ClockSections sections = sectionsFactory.getClockSections(SectionTypes.HOURS);
        Assert.assertArrayEquals(new SectionColors[]{SectionColors.YELLOW}, sections.getMainSection());
        Assert.assertArrayEquals(new SectionColors[]{SectionColors.WHITE}, sections.getNestedSection());
    }
}