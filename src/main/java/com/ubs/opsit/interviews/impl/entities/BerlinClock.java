package com.ubs.opsit.interviews.impl.entities;

import com.ubs.opsit.interviews.impl.sections.feautures.SectionColors;

/**
 * @author evasive
 */
public interface BerlinClock {

    SectionColors[] getMainHours();

    SectionColors[] getNestedHours();

    SectionColors[] getMainMinutes();

    SectionColors[] getNestedMinutes();

    void setSecondsColor(SectionColors color);

    void resetTime();

}
