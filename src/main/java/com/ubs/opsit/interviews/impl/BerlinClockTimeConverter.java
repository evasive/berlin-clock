package com.ubs.opsit.interviews.impl;

import com.ubs.opsit.interviews.TimeConverter;
import com.ubs.opsit.interviews.impl.entities.BerlinClock;
import com.ubs.opsit.interviews.impl.sections.factory.ClockSections;
import com.ubs.opsit.interviews.impl.sections.factory.ClockSectionsPriorityFactory;
import com.ubs.opsit.interviews.impl.sections.factory.DefaultClockSectionsPriorityFactory;
import com.ubs.opsit.interviews.impl.sections.feautures.SectionColors;
import com.ubs.opsit.interviews.impl.sections.feautures.SectionTypes;

/**
 * @author evasive
 */
public class BerlinClockTimeConverter implements TimeConverter {
    private static final String TIME_PARTS_SPLITTER = ":";
    private static final int HOURS_POSITION = 0;
    private static final int MINUTES_POSITION = 1;
    private static final int SECONDS_POSITION = 2;


    private BerlinClock clock;

    public BerlinClockTimeConverter(BerlinClock clock) {
        this.clock = clock;
    }

    @Override
    public String convertTime(String time) {
        String[] timeParts = time.split(TIME_PARTS_SPLITTER);
        clock.resetTime();
        this.populateSeconds(timeParts[SECONDS_POSITION]);
        this.populateHours(timeParts[HOURS_POSITION]);
        this.populateMinutes(timeParts[MINUTES_POSITION]);
        return clock.toString();
    }

    private void populateSeconds(String secondsString) {
        int seconds = Integer.parseInt(secondsString);
        clock.setSecondsColor(isEvenNumber(seconds) ? SectionColors.WHITE : SectionColors.YELLOW);
    }

    private boolean isEvenNumber(int number) {
        return number % 2 == 1;
    }

    private void populateHours(String hours) {
        this.fillBerlinSections(hours, SectionTypes.HOURS);
    }

    private void populateMinutes(String minutes) {
        this.fillBerlinSections(minutes, SectionTypes.MINUTES);
    }

    private void fillBerlinSections(String timePartAsString, SectionTypes sectionType) {
        ClockSectionsPriorityFactory clockSectionsPriorityFactory = new DefaultClockSectionsPriorityFactory(clock);
        ClockSections clockSections = clockSectionsPriorityFactory.getClockSections(sectionType);
        int timePart = Integer.parseInt(timePartAsString);
        fillMainSections(timePart, sectionType, clockSections);
        fillNestedSections(timePart, sectionType, clockSections);
    }

    private void fillMainSections(Integer timePart, SectionTypes sectionType, ClockSections clockSections) {
        int timeDiv = timePart / 5;
        for (int i = 0; i < timeDiv; i++) {
            if (SectionTypes.MINUTES.equals(sectionType) && isQuarterLamp(i)) {
                clockSections.getMainSection()[i] = SectionColors.YELLOW;
            } else {
                clockSections.getMainSection()[i] = SectionColors.RED;
            }
        }
    }

    private void fillNestedSections(Integer timePart, SectionTypes sectionType, ClockSections clockSections) {
        int timeMod = timePart % 5;
        for (int i = 0; i < timeMod; i++) {
            if (SectionTypes.HOURS.equals(sectionType)) {
                clockSections.getNestedSection()[i] = SectionColors.RED;
            } else {
                clockSections.getNestedSection()[i] = SectionColors.YELLOW;
            }
        }
    }

    private boolean isQuarterLamp(int index) {
        return (index + 1) % 3 != 0;
    }

}
