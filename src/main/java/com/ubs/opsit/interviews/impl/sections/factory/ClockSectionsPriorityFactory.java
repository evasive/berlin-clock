package com.ubs.opsit.interviews.impl.sections.factory;

import com.ubs.opsit.interviews.impl.sections.feautures.SectionTypes;

public interface ClockSectionsPriorityFactory {

    ClockSections getClockSections(SectionTypes sectionType);

}
