package com.ubs.opsit.interviews.impl.sections.factory;


import com.ubs.opsit.interviews.impl.entities.BerlinClock;
import com.ubs.opsit.interviews.impl.sections.feautures.SectionColors;
import com.ubs.opsit.interviews.impl.sections.feautures.SectionTypes;

public class DefaultClockSectionsPriorityFactory implements ClockSectionsPriorityFactory {

    private BerlinClock clock;

    public DefaultClockSectionsPriorityFactory(BerlinClock clock) {
        this.clock = clock;
    }

    @Override
    public ClockSections getClockSections(SectionTypes sectionType) {
        if (SectionTypes.MINUTES.equals(sectionType)) {
            return new ClockSections() {
                @Override
                public SectionColors[] getMainSection() {
                    return clock.getMainMinutes();
                }

                @Override
                public SectionColors[] getNestedSection() {
                    return clock.getNestedMinutes();
                }
            };
        } else {
            return new ClockSections() {
                @Override
                public SectionColors[] getMainSection() {
                    return clock.getMainHours();
                }

                @Override
                public SectionColors[] getNestedSection() {
                    return clock.getNestedHours();
                }
            };
        }
    }
}
