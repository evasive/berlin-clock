package com.ubs.opsit.interviews.impl.sections.feautures;

/**
 * @author evasive
 */
public enum SectionTypes {

    MINUTES,
    HOURS

}
