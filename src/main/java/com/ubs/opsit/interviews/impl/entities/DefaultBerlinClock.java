package com.ubs.opsit.interviews.impl.entities;

import com.ubs.opsit.interviews.impl.sections.feautures.SectionColors;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evasive
 */
public class DefaultBerlinClock implements BerlinClock{

    private static final int DEFAULT_SECTIONS_SIZE = 4;
    private static final int MAIN_MINUTES_SECTION_SIZE = 11;

    private SectionColors seconds;
    private SectionColors[] mainHours;
    private SectionColors[] nestedHours;
    private SectionColors[] mainMinutes;
    private SectionColors[] nestedMinutes;
    private List<SectionColors[]> sections;

    public DefaultBerlinClock() {
        this.initSelections();
    }

    private void initSelections() {
        this.seconds = SectionColors.WHITE;
        this.mainHours = createSection(DEFAULT_SECTIONS_SIZE);
        this.nestedHours = createSection(DEFAULT_SECTIONS_SIZE);
        this.mainMinutes = createSection(MAIN_MINUTES_SECTION_SIZE);
        this.nestedMinutes = createSection(DEFAULT_SECTIONS_SIZE);
        this.sections = new ArrayList<SectionColors[]>() {{
            add(mainHours);
            add(nestedHours);
            add(mainMinutes);
            add(nestedMinutes);
        }};
    }

    public void setSecondsColor(SectionColors color) {
        this.seconds = color;
    }

    public SectionColors[] getMainHours() {
        return mainHours;
    }

    public SectionColors[] getNestedHours() {
        return nestedHours;
    }

    public SectionColors[] getMainMinutes() {
        return mainMinutes;
    }

    public SectionColors[] getNestedMinutes() {
        return nestedMinutes;
    }

    public void resetTime() {
        this.initSelections();
    }

    public String toString() {
        StringBuilder berlinClockOutput = new StringBuilder();
        berlinClockOutput.append(seconds.getColor());
        for (SectionColors[] s : sections) {
            berlinClockOutput.append(System.lineSeparator());
            for (SectionColors section_color : s) {
                berlinClockOutput.append(section_color.getColor());
            }
        }
        return berlinClockOutput.toString();
    }

    private SectionColors[] createSection(int size) {
        SectionColors[] section = new SectionColors[size];
        for (int i = 0; i < size; i++) {
            section[i] = SectionColors.WHITE;
        }
        return section;
    }
}
