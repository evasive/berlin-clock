package com.ubs.opsit.interviews.impl.sections.factory;

import com.ubs.opsit.interviews.impl.sections.feautures.SectionColors;

/**
 * @author evasive
 */
public interface ClockSections {

    SectionColors[] getMainSection();

    SectionColors[] getNestedSection();

}
