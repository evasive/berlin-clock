package com.ubs.opsit.interviews.impl.sections.feautures;

/**
 * @author evasive on 22.06.16
 */
public enum SectionColors {

    YELLOW("Y"),
    RED("R"),
    WHITE("O");

    private String color;

    public String getColor() {
        return color;
    }

    SectionColors(String color) {
        this.color = color;
    }

}
